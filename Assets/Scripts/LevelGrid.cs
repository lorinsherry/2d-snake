﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey;


public class LevelGrid
{
    private Vector2Int foodGridPosition;
    private GameObject foodGameObject;
    private int width;
    private int height;
    private Snake snake;

    //when i instantiated the level grid i call the spawn food which generating food
    public LevelGrid(int width, int height)
    {
        this.width = width;
        this.height = height;
                
    }

    public void Setup(Snake snake)
    {
        this.snake = snake;
        //as soon as the level starts initiallize food for the snake :-)
        SpawnFood();
    }

    private void SpawnFood()
    {
        //check the position is valid
        // which means the food will nexer be on top of the snake
        do
        {
            //generates position of apple when the snake ate the last
            foodGridPosition = new Vector2Int(Random.Range(0, width), Random.Range(0, height));
        } while (snake.GetFullSnakeGridPositionList().IndexOf(foodGridPosition) != -1);

        
        // food game object
        foodGameObject = new GameObject("Food", typeof(SpriteRenderer));

        // using the correct sprite
        foodGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.i.foodSprite;

        // to the correct position
        foodGameObject.transform.position = new Vector3(foodGridPosition.x, foodGridPosition.y);
    }

    // and in this function it recieved the snake grid position and i know the food grid position
    //if both them match - the snake ate the food
    public bool TrySnakeEatFood(Vector2Int snakeGridPosition)
    { 
        if (snakeGridPosition == foodGridPosition) {
            //if so, we destroy the game object(apple)
            Object.Destroy(foodGameObject);
            //and we spawn another piece of food
            SpawnFood();
            return true;
        }else
        {
            return false;
        }
       
    }
}

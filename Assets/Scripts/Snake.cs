﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey;
using CodeMonkey.Utils;

public class Snake : MonoBehaviour
{

    private enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }

    private Direction gridMoveDirection;
    private Vector2Int gridPosition;
    private float gridMoveTimer;
    private float gridMoveTimerMax;
    private LevelGrid levelGrid;
    //where to add it
    private List<SnakeMovePosition> snakeMovePositionList;

    private List<SnakeBodyPart> SnakeBodyPartList;

    //add size to snake body
    private int snakeBodySize;
   

    public void Setup(LevelGrid levelGrid)
    {
        this.levelGrid = levelGrid;
    }

    private void Awake()
    {

        gridPosition = new Vector2Int(10, 10);
        // every second my snake will move on the grid
        gridMoveTimerMax = .2f;
        gridMoveTimer = gridMoveTimerMax;

        //by default my snake will movin to the right ->
        gridMoveDirection = Direction.Right;

        snakeMovePositionList = new List<SnakeMovePosition>();
        snakeBodySize = 0;

        SnakeBodyPartList = new List<SnakeBodyPart>();
     }

    private void Update()
    {
       HandleInput();
       HandleGridMovement();
}

    private void HandleInput()
    {
        // *** WHEN CLICKING SOME KEY MOVEMENT, 
        // *** THE SNAKE KEEP MOVING ACCORDING TO THE RELEVANT KEY

        //when the snake goes up, the y-axis goes up
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //gridPosition.x += 1 ------> This wont interface with the time flow

            //we can only go up if we are not going down
            if (gridMoveDirection != Direction.Down)
            {
                gridMoveDirection = Direction.Up;
            }
        }
        //when the snake goes down, the y-axis goes down a well
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            //we can only go down if we are not going up
            if (gridMoveDirection != Direction.Up)
            {
                gridMoveDirection = Direction.Down;
            }

        }
        //when the snake goes left, the x-axis goes down
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //we can only go right if we are not going left
            if (gridMoveDirection != Direction.Right)
            {
                gridMoveDirection = Direction.Left;
            }
        }
        //when the snake goes right, the x-axis goes up
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //we can only go left if we are not going right
            if (gridMoveDirection != Direction.Left)
            {
                gridMoveDirection = Direction.Right;
            }
        }
    }

    private void HandleGridMovement()
    {
        //so it wont move by frame(depending my hardware), it will move by actuall time.
        gridMoveTimer += Time.deltaTime;
        if (gridMoveTimer >= gridMoveTimerMax)
        {
            gridMoveTimer -= gridMoveTimerMax;

            SnakeMovePosition snakeMovePosition = new SnakeMovePosition(gridPosition, gridMoveDirection);
            snakeMovePositionList.Insert(0, snakeMovePosition);

            Vector2Int gridMoveDirectionVector;
            switch (gridMoveDirection)
            {
                default:
                case Direction.Right: gridMoveDirectionVector = new Vector2Int(+1, 0); break;
                case Direction.Left:  gridMoveDirectionVector = new Vector2Int(-1, 0); break;
                case Direction.Up:    gridMoveDirectionVector = new Vector2Int(0, +1); break;
                case Direction.Down:  gridMoveDirectionVector = new Vector2Int(0, -1); break;
            }

            gridPosition += gridMoveDirectionVector;


            //after it moves it calls the snakeMoved function on the level grid
            bool snakeAteFood = levelGrid.TrySnakeEatFood(gridPosition);
            if (snakeAteFood)
            {
                //snake ate food, grow body
                snakeBodySize++;
                CreateSnakeBodyPart();
            }

            //test if the size is too big according to body size
            if (snakeMovePositionList.Count >= snakeBodySize + 1)
            {
                snakeMovePositionList.RemoveAt(snakeMovePositionList.Count - 1);
            }

            /*for(int i = 0; i < snakeMovePositionList.Count; i++)
            {
                Vector2Int snakeMovePosition = snakeMovePositionList[i];
                World_Sprite worldSprite = World_Sprite.Create(new Vector3(snakeMovePosition.x, snakeMovePosition.y), Vector3.one * .5f, Color.white);
                FunctionTimer.Create(worldSprite.DestroySelf, gridMoveTimerMax);
            }*/

            transform.position = new Vector3(gridPosition.x, gridPosition.y);
            transform.eulerAngles = new Vector3(0, 0, GetAngleFromVector(gridMoveDirectionVector) - 90);

            UpdateSnakeBodyParts();
           

        }
        
    }
    
    private void CreateSnakeBodyPart()
    {
        SnakeBodyPartList.Add(new SnakeBodyPart(SnakeBodyPartList.Count));
    }

    private void UpdateSnakeBodyParts()
    {
        for (int i = 0; i < SnakeBodyPartList.Count; i++)
        {
            SnakeBodyPartList[i].SetSnakeMovePosition(snakeMovePositionList[i]);
        }
    }

    private float GetAngleFromVector(Vector2Int dir)
    {
        //takes the vectornt and return it float to get the angle
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;
        return n;
    }

    public Vector2Int GetGridPosition()
    {
        return gridPosition;
    }

    // return the full list of positions occupie by the snake head and body
    public List<Vector2Int> GetFullSnakeGridPositionList()
    {
        List<Vector2Int> gridPositionList = new List<Vector2Int>()
        {
            gridPosition //the head
        };
        foreach (SnakeMovePosition snakeMovePosition in snakeMovePositionList)
        {
            gridPositionList.Add(snakeMovePosition.GetGridPosition());
        }
        
        return gridPositionList;
    }

    private class SnakeBodyPart
   
    {
        private SnakeMovePosition snakeMovePosition;
        private Transform transform;
        public SnakeBodyPart(int bodyIndex)
        {
            GameObject snakeBodyGameObject = new GameObject("SnakeBody", typeof(SpriteRenderer));
            snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.i.snakeBodySprite;
            snakeBodyGameObject.GetComponent<SpriteRenderer>().sortingOrder = -bodyIndex;
            transform = snakeBodyGameObject.transform;
        }

        public void SetSnakeMovePosition(SnakeMovePosition snakeMovePosition)
        {
            this.snakeMovePosition = snakeMovePosition;
            transform.position = new Vector3(snakeMovePosition.GetGridPosition().x, snakeMovePosition.GetGridPosition().y);


            float angle;
            switch(snakeMovePosition.GetDirection()){
                default:
                case Direction.Up:
                    angle = 0;
                    break;
                case Direction.Down:
                    angle = 180;
                    break;
                case Direction.Left:
                    angle = -90;
                    break;
                case Direction.Right:
                    angle = 90;
                    break;

            }
            transform.eulerAngles = new Vector3(0, 0, angle);
        }
    }

    // store one single move postion from the snake
    private class SnakeMovePosition
    {
        private Vector2Int gridPosition;
        private Direction direction;

        public SnakeMovePosition(Vector2Int gridPosition, Direction direction)
        {
            this.gridPosition = gridPosition;
            this.direction = direction;
        }

        public Vector2Int GetGridPosition()
        {
            return gridPosition;
        }

        public Direction GetDirection()
        {
            return direction;
        }

    }




}
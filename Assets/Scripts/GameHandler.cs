﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey;
using CodeMonkey.Utils;

public class GameHandler : MonoBehaviour {

    [SerializeField] private Snake snake;
    private LevelGrid levelGrid;
    private void Start()
    {
        Debug.Log("GameHandler.Start");
        /**
        GameObject snakeHeadGameObject = new GameObject();
        SpriteRenderer snakeSpriteRenderer = snakeHeadGameObject.AddComponent<SpriteRenderer>();

        //assets reference to my code
        snakeSpriteRenderer.sprite = GameAssets.i.snakeHeadSprite;
        **/
        levelGrid = new LevelGrid(20, 20);

        snake.Setup(levelGrid);
        levelGrid.Setup(snake);
    }
       

}
